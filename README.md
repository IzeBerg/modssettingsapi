# ModsSettings API

This modification provides an API for third party developers to create settings for other modifications inside game easily.  
Requires [ModsList API](https://gitlab.com/wot-public-mods/mods-list) as a dependency to allow opening the settings window.

## Preview

![alt text](assets/preview_modslist.png)
![Main Window preview](assets/preview_window.png)
