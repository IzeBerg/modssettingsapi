﻿package poliroid.gui.lobby.modsSettings.utils
{

	public class Constants extends Object
	{
		public static const WINDOW_BACKGROUND_IMAGE:String = '../../gui/maps/uiKit/dialogs/noize_bg.png';

		public static const MOD_COMPONENT_WIDTH:Number = 900;

		public static const COMPONENT_HEADER_MARGIN:Number = 20;
		public static const COMPONENT_MARGIN_BOTTOM:Number = 10;

		public static const RADIO_BUTTONS_MARGIN:Number = 20;

		public static const MOD_PADDING_TOP:Number = 20;
		public static const MOD_PADDING_BOTTOM:Number = 20;
		public static const MOD_PADDING_LEFT:Number = 20;
		public static const MOD_MARGIN_BOTTOM:Number = 5;

		public static const EMPTY_COMPONENT_HEIGHT:Number = 20;

		public static const SLIDER_VALUE_KEY:String = "{{value}}";
		public static const SLIDER_VALUE_MARGIN:Number = 10;

		public static const BUTTON_MARGIN_LEFT:Number = 10;

		public static const COMPONENT_RETURN_VALUE_KEY:String = "returnValue";

		public static const MAX_BOTTOM_OFFSET:int = 230;

		public static const HOTKEY_CONTEXT_MENU_HANDLER:String = 'modsSettingsHotkeyContextMenuHandler';

		public function Constants()
		{
			super();
		}
	}
}
